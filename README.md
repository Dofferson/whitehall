# Whitehall

Arduino sketch for setting the TX/RX LO frequencies for the Pye Whitehall sets.

Requires: Si5351 libray from Jason Milldrum <milldrum@gmail.com>  
          https://github.com/etherkit/Si5351Arduino
   
This sketch uses the S15351 breakout board to generate LO frequencies for 10 selectable channels
( digital pin 2 ... 11). 

Whenever the channel selector switch is changed the Si5351 is instructed to output a signal on a frequency based on
the RX and / or TX frequencies defined in the `channels` array:
  
RX:
  
Output   :  Si5351 CLK0
Frequency: (freq+10.7)/2
   
TX:  
 
Output   :  Si5351 CLK1
Frequency:  freq/12

When PTT button is pressed the CLK1 frequency is toggeld between the calculated TX frequency (PTT button pressed) and 
the TX Park frequency (PTT button released)
 
Both the channel and PTT input pins are activve HIGH meaning they should be pulled low using a pulldown resistor (i.e. 10K)
 
Debug information is echoed on the terminal on 9600 Bd
 
73, Dave Hoebe PA5DOF