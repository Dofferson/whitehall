/* 
 *  Whitehall.ino 
 *  
 *  Version : 2.0.0 - 20-07-2020
 *  
 *  Requires: Si5351 libray from Jason Milldrum <milldrum@gmail.com>  
 *            https://github.com/etherkit/Si5351Arduino
 *  
 *  This sketch uses the S15351 breakout board to generate LO frequencies for 10 selectable channels
 *  ( digital pin 2 ... 11). 
 * 
 *  Whenever the channel selector switch is changed the Si5351 is instructed to output a signal on a frequency based on
 *  the RX and / or TX frequencies defined in the `channels` array:
 *  
 *  RX:
 *  
 *    Output   :  Si5351 CLK0
 *    Frequency: (freq+10.7)/2
 *    
 *  TX:  
 *  
 *    Output   :  Si5351 CLK1
 *    Frequency:  freq/12
 * 
 *  When PTT button is pressed the CLK1 frequency is toggeld between the calculated TX frequency (PTT button pressed) and 
 *  the TX Park frequency (PTT button released)
 *  
 *  Both the channel and PTT input pins are activve HIGH meaning they should be pulled low using a pulldown resistor (i.e. 10K)
 *  
 *  Debug information is echoed on the terminal on 9600 Bd
 *  
 *  73, Dave Hoebe PA5DOF
 *  
 *  
 *  CHANGELOG:
 *  
 *  1.0.0 09-09-2018  - original 10 channel version
 *  2.0.0 20-07-2020  - changed inputs to use 2 bcd encoders allowing 100 channels to be set
 */
 
#include <avr/pgmspace.h>
#include <si5351.h>
#include "Wire.h"

// BCD selector A (tens) pins
// Since i'm lazy these pins should be consecutive
#define BCD_A_1_PIN   2
#define BCD_A_2_PIN   3
#define BCD_A_3_PIN   4
#define BCD_A_4_PIN   5
// BCD selector B (singles) pins
// Since i'm lazy these pins should be consecutive
#define BCD_B_1_PIN   6
#define BCD_B_2_PIN   7
#define BCD_B_3_PIN   8
#define BCD_B_4_PIN   9

#define PTT_PIN         12
#define CTCSS_PIN       13

// Channel structure
typedef struct {
  double rxfreq;    // RX frequency in Mhz
  double txfreq;    // TX frequency in Mhz
  uint8_t ctcss;    // CTCSS frequency (0 = disable)
} channel;

/**
 *  Define channel 0 ... 99 here.
 *  
 *  The number of channels defined here should match the max value that can be selected in your hardware (the bcd encoders).
 *    
 *  So if you have a 2 digit BCD selector you NEED to define 100 channels here !
 *  
 *  first value = rx frequency
 *  second value = tx frequency
 *  third value = ctcss tone frequency
 */
const channel channel_0 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_1 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_2 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_3 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_4 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_5 PROGMEM = {70.150, 70.150, 77};
const channel channel_6 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_7 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_8 PROGMEM = {70.225, 70.225, 77};
const channel channel_9 PROGMEM = {70.250, 70.250, 77}; 

const channel channel_10 PROGMEM = {70.275, 70.275, 77}; 
const channel channel_11 PROGMEM = {70.300, 70.300, 150}; 
const channel channel_12 PROGMEM = {70.325, 70.325, 77}; 
const channel channel_13 PROGMEM = {70.350, 70.350, 77}; 
const channel channel_14 PROGMEM = {70.375, 70.375, 77}; 
const channel channel_15 PROGMEM = {70.400, 70.400, 77};
const channel channel_16 PROGMEM = {70.425, 70.425, 77}; 
const channel channel_17 PROGMEM = {70.450, 70.450, 77}; 
const channel channel_18 PROGMEM = {70.475, 70.475, 77};  
const channel channel_19 PROGMEM = {70.500, 70.500, 77};  

const channel channel_20 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_21 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_22 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_23 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_24 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_25 PROGMEM = {70.150, 70.150, 77};
const channel channel_26 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_27 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_28 PROGMEM = {70.225, 70.225, 77};
const channel channel_29 PROGMEM = {70.250, 70.250, 77}; 

const channel channel_30 PROGMEM = {70.275, 70.275, 77}; 
const channel channel_31 PROGMEM = {70.300, 70.300, 150}; 
const channel channel_32 PROGMEM = {70.325, 70.325, 77}; 
const channel channel_33 PROGMEM = {70.350, 70.350, 77}; 
const channel channel_34 PROGMEM = {70.375, 70.375, 77}; 
const channel channel_35 PROGMEM = {70.400, 70.400, 77};
const channel channel_36 PROGMEM = {70.425, 70.425, 77}; 
const channel channel_37 PROGMEM = {70.450, 70.450, 77}; 
const channel channel_38 PROGMEM = {70.475, 70.475, 77};  
const channel channel_39 PROGMEM = {70.500, 70.500, 77}; 

const channel channel_40 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_41 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_42 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_43 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_44 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_45 PROGMEM = {70.150, 70.150, 77};
const channel channel_46 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_47 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_48 PROGMEM = {70.225, 70.225, 77};
const channel channel_49 PROGMEM = {70.250, 70.250, 77}; 

const channel channel_50 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_51 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_52 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_53 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_54 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_55 PROGMEM = {70.150, 70.150, 77};
const channel channel_56 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_57 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_58 PROGMEM = {70.225, 70.225, 77};
const channel channel_59 PROGMEM = {70.250, 70.250, 77}; 

const channel channel_60 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_61 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_62 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_63 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_64 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_65 PROGMEM = {70.150, 70.150, 77};
const channel channel_66 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_67 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_68 PROGMEM = {70.225, 70.225, 77};
const channel channel_69 PROGMEM = {70.250, 70.250, 77}; 

const channel channel_70 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_71 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_72 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_73 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_74 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_75 PROGMEM = {70.150, 70.150, 77};
const channel channel_76 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_77 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_78 PROGMEM = {70.225, 70.225, 77};
const channel channel_79 PROGMEM = {70.250, 70.250, 77}; 

const channel channel_80 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_81 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_82 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_83 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_84 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_85 PROGMEM = {70.150, 70.150, 77};
const channel channel_86 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_87 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_88 PROGMEM = {70.225, 70.225, 77};
const channel channel_89 PROGMEM = {70.250, 70.250, 77}; 

const channel channel_90 PROGMEM = {70.025, 70.025, 77}; 
const channel channel_91 PROGMEM = {70.050, 70.050, 150}; 
const channel channel_92 PROGMEM = {70.075, 70.075, 77}; 
const channel channel_93 PROGMEM = {70.100, 70.100, 77}; 
const channel channel_94 PROGMEM = {70.125, 70.125, 77}; 
const channel channel_95 PROGMEM = {70.150, 70.150, 77};
const channel channel_96 PROGMEM = {70.175, 70.175, 77}; 
const channel channel_97 PROGMEM = {70.200, 70.200, 77}; 
const channel channel_98 PROGMEM = {70.225, 70.225, 77};
const channel channel_99 PROGMEM = {70.250, 70.250, 77}; 

const channel channels[] PROGMEM =     
{   
  channel_0, channel_1, channel_2, channel_3, channel_4, channel_5, channel_6, channel_7, channel_8, channel_9,
  channel_10, channel_11, channel_12, channel_13, channel_14, channel_15, channel_16, channel_17, channel_18, channel_19,
  channel_20, channel_21, channel_22, channel_23, channel_24, channel_25, channel_26, channel_27, channel_28, channel_29,
  channel_30, channel_31, channel_32, channel_33, channel_34, channel_35, channel_36, channel_37, channel_38, channel_39,
  channel_40, channel_41, channel_42, channel_43, channel_44, channel_45, channel_46, channel_47, channel_48, channel_49,
  channel_50, channel_51, channel_52, channel_53, channel_54, channel_55, channel_56, channel_57, channel_58, channel_59,
  channel_60, channel_61, channel_62, channel_63, channel_64, channel_65, channel_66, channel_67, channel_68, channel_69,
  channel_70, channel_71, channel_72, channel_73, channel_74, channel_75, channel_76, channel_77, channel_78, channel_79,
  channel_80, channel_81, channel_82, channel_83, channel_84, channel_85, channel_86, channel_87, channel_88, channel_89,
  channel_90, channel_91, channel_92, channel_93, channel_94, channel_95, channel_96, channel_97, channel_98, channel_99
};

channel selectedChannel;

// TX park fequency in Mhz.
const double txParkFreq  = 4.3030723;

// Button state variables.
// these are declared as 'volatile' to make sure they can be accessed from interrupt calls.
volatile uint8_t  selectedChannelNr   = 0;
volatile uint8_t  tmpChannelNr        = 0;
volatile bool     channelChanged      = false;
volatile bool     pttState            = false;
volatile bool     pttChanged          = false;

// init a new instance of the Si5351 library
Si5351 si5351;

void setup() {
  // for debugging
  Serial.begin(9600);
  
  // Init the Si5351                          
  while(!si5351.init(SI5351_CRYSTAL_LOAD_8PF, 0, 0)){
    Serial.println("There is a problem initiating th si5351");
    delay(500);
  }

  // Set the Si5351  correction value
  // Use this to calibrate the si5351, see Si5351_calibration sketch supplied in the example section
  // This value differs for every chip and needs to be changed.   
  si5351.set_correction(-4500, SI5351_PLL_INPUT_XO);
  
  // Set the Si5351 drive level
  //
  // 2mA = ~ 3dbm, 8mA = ~ 10dbm (on 50 Ohm load)
  //
  // Valid values are:
  //  SI5351_DRIVE_2MA 
  //  SI5351_DRIVE_4MA
  //  SI5351_DRIVE_6MA
  //  SI5351_DRIVE_8MA
  si5351.drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA);
  si5351.drive_strength(SI5351_CLK1, SI5351_DRIVE_2MA);

  // Setup the channel selector input pins
  // 'tens' BCD encoder
  pinSetup(BCD_A_1_PIN);
  pinSetup(BCD_A_2_PIN);
  pinSetup(BCD_A_3_PIN);
  pinSetup(BCD_A_4_PIN);
  // 'singles' BCD encoder
  pinSetup(BCD_B_1_PIN);
  pinSetup(BCD_B_2_PIN);
  pinSetup(BCD_B_3_PIN);
  pinSetup(BCD_B_4_PIN);
    
  // Setup the PPT pin
  pinSetup(PTT_PIN);

  // check the intial setting of the channel switch
  scanInputs();
  
  Serial.println("Setup done..");
}

/**
 * Main program loop
 */
void loop() {

  // Change Frequencies for CLK0 en CLK1 when the state of the
  // PTT button has changed
  if (pttChanged) {
    setFrequency(selectedChannelNr, pttState);
    delay(100); // debounce
    pttChanged = false;
  }

  // Change Frequencies for CLK0 en CLK1 when the state of the
  // Channel selector has changed 
  if (channelChanged) {
      // wait till the BCD encoder is settled
      // This is done by checking the BCD encoder(s) until their value is the 
      // same within a defined period (i.e. 500 mS)
      uint8_t tmp;
      while(tmp != tmpChannelNr) {
        tmp = getChannel();
        // wait this period (milli seconds) to check the setting of the BCD
        // encoder(s) again. you can raise this when changing channels behaves strangely. i.e. selecting channel 4 when changing from channel 6 to 7
        delay(250);
      }

     // when the selected channel differs from the current channel/
      if (tmp != selectedChannelNr) {
        Serial.println();
        selectedChannelNr = tmp;
        setFrequency(selectedChannelNr ,pttState);
      }
      
      channelChanged = false;

      delay(10);  // debounce
  }
}

/**
 * Set the Si5351 frequency
 * 
 * This function sets the CLK0 (RX) and CLK1 (TX) frequency as wel as the
 * ctcss tone generated on the CTCSS_PIN
 */
void setFrequency(uint8_t chan, bool ptt) {
  
  // TX = freq/12
  // RX = (freq+10.7)/2

  // copy channel struct from progmem to ram
  memcpy_P(&selectedChannel, &channels[chan], sizeof(channel));
 
  unsigned long rxFreqHz;
  unsigned long txFreqHz;

  // calculate CLK0 (RX freq)
  // Since the Arduino isn't precise enough for floating point calculations the floating point values (Mhz)
  // are converted to unsigned long values (hz)
  rxFreqHz = ((selectedChannel.rxfreq * 1000000UL) + 10700000UL) / 2;
  
  if (ptt) {
    // calculate CLK1 (TX freq)
    txFreqHz = (selectedChannel.txfreq * 1000000UL) / 12;
    // Set tone
    setTone(chan);
  } else {
    // if ptt isn't pressed set TX CLK1 to park frequency.
    // it is assumed that the park frequency is always the same.
    txFreqHz = txParkFreq * 1000000UL;
    noTone(CTCSS_PIN);
  }

  // Frequencies need to be set in 1/100 of hz  
  si5351.set_freq(rxFreqHz*100, SI5351_CLK0);
  si5351.set_freq(txFreqHz*100, SI5351_CLK1);

  // Debug
  Serial.print(chan);
  Serial.print(" : RX freq: ");
  Serial.print(selectedChannel.rxfreq, 3);
  Serial.print(" = CLK0 freq: ");
  Serial.print(rxFreqHz / 1000000.0, 6);
  Serial.print(", TX freq: ");
  if (ptt) {
    Serial.print(selectedChannel.txfreq, 3);
  } else {
    Serial.print("---");
  }
  Serial.print(" = CLK1 freq: ");
  Serial.print(txFreqHz / 1000000.0, 6);
  Serial.print(", CTCSS: ");
  if (ptt && selectedChannel.ctcss) {
    Serial.println(selectedChannel.ctcss);
  } else {
    Serial.println("---");
  }

}

/**
 * Generates a 50% duty cycle square wave on the CTCSS_PIN.
 * 
 * The frequency of the square wave is defined in the ctcss property of the
 * channels array.
 */
void setTone(uint8_t chan) {

   // copy channel struct from progmem to ram
   memcpy_P(&selectedChannel, &channels[chan], sizeof(channel));
  
   if (selectedChannel.ctcss) {
     tone(CTCSS_PIN, selectedChannel.ctcss);
   } else {
     noTone(CTCSS_PIN);
   }
}

/**
 * Setup pin as input pin and register it as an interrupt pin
 */
void pinSetup(byte pin) {

  // Set pin to input
  pinMode(pin, INPUT);

  // Register an interrupt for selected pin
  *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));
  PCIFR  |= bit (digitalPinToPCICRbit(pin));
  PCICR  |= bit (digitalPinToPCICRbit(pin)); 
  
}

/**
 * Returns the channel number (decimal) set by the BCD encoders
 * 
 * @return uint8_t
 */
uint8_t getChannel() {
  uint8_t tens = 0;
  uint8_t singles = 0;

  // Left digit (tens)
  for (uint8_t lusa = BCD_A_1_PIN; lusa <= BCD_A_4_PIN; lusa++) {  
    if (digitalRead(lusa)) {
      bitSet(tens , (lusa-2));
    } 
  }

  // Right digit (singles) 
  for (uint8_t lusa = BCD_B_1_PIN; lusa <= BCD_B_4_PIN; lusa++) {  
    if (digitalRead(lusa)) {
      bitSet(singles , (lusa-6));
    } 
  }
  
  return (tens*10)+singles;
}

/**
 * This function is called when an interrupt occurred on one of the channel BCD selector
 * pins or the ptt pin.
 */
void scanInputs() {
 
  // check if we have a PTT change
  bool ptt = digitalRead(PTT_PIN);
  
  if (pttState != ptt) {
    //Serial.print(ptt);
    pttState = ptt;
    pttChanged = true;
  } else {
    // Get the channel number of the two BCD encoders
    tmpChannelNr = getChannel();
    // Mark the channel changed
    channelChanged = true;
  }
}


// Interrupt handler for pin D8 - D13
// When a state change is detected on these pins it will execute the scanInputs() function
ISR (PCINT0_vect)
{    
  scanInputs();
}

// Interrupt handler for pin D0 - D7
// When a state change is detected on these pins it will execute the scanInputs() function
ISR (PCINT2_vect) 
{
  scanInputs();
}  
